{
    "id": "8d4cd795-afd3-4689-87c8-9d0df0a159ff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_snake_body",
    "eventList": [
        {
            "id": "0dca1191-b9ba-4da7-bc1d-4d5d2ef49abf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8d4cd795-afd3-4689-87c8-9d0df0a159ff"
        },
        {
            "id": "b17f1eb8-73d6-4443-b57e-e99ca81f5cd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8d4cd795-afd3-4689-87c8-9d0df0a159ff"
        },
        {
            "id": "83c28f70-d1c7-48db-9ef1-96d9910e4d6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8d4cd795-afd3-4689-87c8-9d0df0a159ff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bb461a8b-f659-4d3d-acac-6731650fc8a5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "407ce3aa-2d2a-41e2-ad1a-71fa16d59822",
    "visible": true
}