
if (controller.tc <= 0)
{	
	//movement
	x = sn_for.last_x;
	y = sn_for.last_y;
	//image_index -= 1;
}

if (instance_number(obj_snake_body) < snake.stc)
{
	if (!sn_have_spawned)
	{
		sn_pre = instance_create_layer(x,y, "player", obj_snake_body);
		with (sn_pre)
		{
			sn_for = other.id;
			x = other.last_x;
			y = other.last_y;
			image_index = sn_for.image_index + 1;
		}
	
		sn_have_spawned = true;
	}
}

//image_index = sn_for.image_index + 1;

