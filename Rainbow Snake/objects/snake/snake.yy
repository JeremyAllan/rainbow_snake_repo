{
    "id": "bb461a8b-f659-4d3d-acac-6731650fc8a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "snake",
    "eventList": [
        {
            "id": "f441f7c2-78d4-45c7-b14d-129352d9a82b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bb461a8b-f659-4d3d-acac-6731650fc8a5"
        },
        {
            "id": "0fe22636-dce0-4d7c-b403-0157bde60a1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bb461a8b-f659-4d3d-acac-6731650fc8a5"
        },
        {
            "id": "91e407da-c0a4-43ad-9d51-7ada35223227",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "bb461a8b-f659-4d3d-acac-6731650fc8a5"
        },
        {
            "id": "e93f77e8-3bc2-4b0f-aff7-49fbe91e710f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bb461a8b-f659-4d3d-acac-6731650fc8a5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ae499529-4ebc-4f9d-84f1-766a561e8a9e",
    "visible": true
}