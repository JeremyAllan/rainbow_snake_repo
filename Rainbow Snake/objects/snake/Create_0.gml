/// @description 
//unit = 32;

#region colours
sn_clr_t = 6; // total colours
ar_clr = array_create(sn_clr_t, 0); //create the array
ar_clr_len = array_length_1d(ar_clr);

#region colour array
ar_clr[0] = c_red; // set the colours
ar_clr[1] = c_orange;
ar_clr[2] = c_yellow;
ar_clr[3] = c_green;
ar_clr[4] = c_blue;
ar_clr[5] = c_gray;
ar_clr[6] = c_fuchsia;

#endregion

////sn_clr_start = irandom(sn_clr_t); //random gen a number within the array size for the starting head colour
////show_debug_message("starting head colour picked!");
//sn_clr_start = irandom(sprite_get_number(sprite_index)); //random gen a number within the array size for the starting head colour


/*	This method of picking colour will leave the snake segments as the same when they move,	make version later that changes with every move, ie make the next cell take the colour of the one before it
	ie, pre < > fore */

#endregion

sn_head_x = x;
sn_head_y = y;
sn_clr_start = irandom_range(0, sprite_get_number(sprite_index));
sn_unit_clr = sn_clr_start; // assign random gen colour to head
sn_dir = right;
sn_dir_last = 0;

image_index = 0;
image_speed = 0;

last_x = 0;
last_y = 0;

sn_spawn_x = 0;
sn_spawn_y = 0;
sn_have_spawned = false;

sn_for = 0;		// towards head
sn_pre = 0;		// towards tail
sn_for_x = 0;	// coords of fore piece
sn_for_y = 0;
sn_pre_x = 0;
sn_pre_y = 0;



// moved spawn direction switch to step

#region snake tails
stc_start = 2;
stc = stc_start;

#endregion