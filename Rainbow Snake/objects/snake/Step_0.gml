/// @description 

//last_x = x;
//last_y = y;

#region keys
k_up = keyboard_check(ord("W"));
k_left = keyboard_check(ord("A"));
k_down = keyboard_check(ord("S"));
k_right = keyboard_check(ord("D"));

kp_up = keyboard_check_pressed(ord("W"));
kp_left = keyboard_check_pressed(ord("A"));
kp_down = keyboard_check_pressed(ord("S"));
kp_right = keyboard_check_pressed(ord("D"));

kr_up = keyboard_check_released(ord("W"));
kr_left = keyboard_check_released(ord("A"));
kr_down = keyboard_check_released(ord("S"));
kr_right = keyboard_check_released(ord("D"));

if kp_up show_debug_message("key up pressed");
if kp_left show_debug_message("key left pressed");
if kp_down show_debug_message("key down pressed");
if kp_right show_debug_message("key right pressed");

//if k_up	y -= 1;
//if k_left x -= 1;
//if k_down y += 1;
//if k_right x += 1;

#endregion

#region directions & head movement
switch (keyboard_key)
{
	case ord("D"):
		if (!place_meeting(x+unit, y, obj_snake_body))
		{
			sn_dir = right;
			sprite_index = spr_sn_head_right; // change this to separate statement so it doesnt switch back when I dont want it to
		}
		break;
	case ord("W"):
		if (!place_meeting(x, y-unit, obj_snake_body))
		{
			sn_dir = up;
			sprite_index = spr_sn_head_up;
		}
		break;
	case ord("A"):
		if (!place_meeting(x-unit, y, obj_snake_body))
		{
			sn_dir = left;
			sprite_index = spr_sn_head_left;
		}
		break;
	case ord("S"):
		if (!place_meeting(x, y+unit, obj_snake_body))
		{
			sn_dir = down;
			sprite_index = spr_sn_head_down;
		}
		break;
}
#endregion

// spawning body parts
if (!sn_have_spawned) && (instance_number(obj_snake_body) < snake.stc)
{
	sn_pre = instance_create_layer(x,y, "player", obj_snake_body);
	with (sn_pre)
	{
		sn_for = other.id;
		x = other.last_x;
		y = other.last_y;
		image_index = sn_for.image_index + 1;
	}
	
	sn_have_spawned = true;
}	


#region ticks
if (controller.tc <= 0)
{
	#region movement
	switch (sn_dir)
	{
		case right:
			if (x >= room_width) {
				x = 0;
			}
			else {
				x += unit;
			}
			break;
		case up:
			if (y <= 0) {
				y = room_height - unit;	
			}
			else {
				y -= unit;
			}
			break;
		case left:
			if (x <= 0) {
				x = room_width - unit;
			}
			else {
				x -= unit;
			}
			break;
		case down:	
			if (y >= room_height) {
				y = 0;
			}
			else {
				y += unit;
			}
			break;
	}
	#endregion
	
	
}
#endregion


#region spawn direction // turned off

//switch (sn_dir) 
//{
//	case right:
//		sn_spawn_x = x-unit;
//		sn_spawn_y = y;
//		break;
//	case up:
//		sn_spawn_x = x;
//		sn_spawn_y = y+unit;
//		break;
//	case left:
//		sn_spawn_x = x+unit;
//		sn_spawn_y = y;
//		break;
//	case down:
//		sn_spawn_x = x;
//		sn_spawn_y = y-unit;
//		break;		
//}
#endregion

#region direction switch & move & spawn // failed 
// direction switch & move
//switch (sn_dir)
//{
//	case right:
//		x += unit;
//		sn_dir_last = right;
//		break;
//	case up:
//		y -= unit;
//		sn_dir_last = up;
//		break;
//	case left:
//		x -= unit;
//		sn_dir_last = left;
//		break;
//	case down:
//		y += unit;
//		sn_dir_last = down;
//		break;
//}
	
//// spawn check
//if (instance_number(obj_snake_body) < snake.stc)
//{
//	sn_unit_pre = instance_create_layer(sn_spawn_x, sn_spawn_y, "player", obj_snake_body);
//	with (sn_unit_pre)
//	{
//		sn_unit_for = other.id;
//		sn_for_x = other.x;
//		sn_for_y = other.y;
			
//		image_index = other.image_index + 1;
//		image_speed = 0;
//	}
	
//	sn_have_spawned = true;
//}
#endregion

