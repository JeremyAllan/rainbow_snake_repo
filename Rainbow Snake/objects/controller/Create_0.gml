/// @description 

randomize();

#macro right 0
#macro up 90
#macro left 180
#macro down 270
#macro unit 32

rmu = unit;
rmw = room_width/rmu;
rmh = room_height/rmu;
gofs = 2;

//tick_rate = delta_time;
tick_rate_start = 0.5;
tick_rate = tick_rate_start; // x secs
tick_counter = 60;
tc = tick_counter * tick_rate;
tc = 30;

spos_x_start = (room_width / 2) - unit;  
spos_y_start = room_height / 2;
//spos_y_start = unit*11;

spos_x = spos_x_start;
spos_y = spos_y_start;

start_sdir = 0;

score_p = 0;
score_inc = 10;

#region creates
//create snake
if (instance_exists(snake) == false)
{
	instance_create_layer(spos_x_start,spos_y_start, "player", snake);
}

//// create food
//if (!instance_exists(food))
//{
//	var nx = (irandom_range(0, room_width)) * unit; 
//	var ny = (irandom_range(0, room_height)) * unit;  

//	Spawn_food(nx,ny, room_width,room_height, "food");
//}

// create level counter
if (instance_exists(obj_levelup_counter) == false)
{
	instance_create_layer(x,y,"ui_layer", obj_levelup_counter);
}

#endregion