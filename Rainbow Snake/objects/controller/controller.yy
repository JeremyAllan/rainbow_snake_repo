{
    "id": "f37aa0b0-d225-4e59-a0a0-a3cfd4825baa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "controller",
    "eventList": [
        {
            "id": "545097d0-74a3-4ab3-8cad-d82aa5fe670c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f37aa0b0-d225-4e59-a0a0-a3cfd4825baa"
        },
        {
            "id": "f5afe3bf-1ab3-4c3b-adc1-7fd904d2bc11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f37aa0b0-d225-4e59-a0a0-a3cfd4825baa"
        },
        {
            "id": "d13efdab-313c-4f57-ac59-14c109556b1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f37aa0b0-d225-4e59-a0a0-a3cfd4825baa"
        },
        {
            "id": "6cd3d5ed-664e-46f5-b26e-c081f18c971e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "f37aa0b0-d225-4e59-a0a0-a3cfd4825baa"
        },
        {
            "id": "2dd4d108-0471-424d-8a8b-afd3af13faff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 84,
            "eventtype": 10,
            "m_owner": "f37aa0b0-d225-4e59-a0a0-a3cfd4825baa"
        },
        {
            "id": "8ce72584-2bf8-48c9-bc8b-5b4e68c21664",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f37aa0b0-d225-4e59-a0a0-a3cfd4825baa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "07b87de7-0886-45f2-b26b-126a8e39d629",
    "visible": true
}