/// @description 


if (tc <= 0)
{
	tc = tick_counter * tick_rate;
}
else tc--;

if (tick_rate > 0.25) tick_rate = tick_rate_start - (snake.stc*0.01);

// create food
if (instance_exists(food) == false) 
{
	var nx = (irandom_range(0, room_width/unit)) * unit; 
	var ny = (irandom_range(0, room_height/unit)) * unit;  

 	Spawn_food(nx,ny, room_width,room_height, "food");
}