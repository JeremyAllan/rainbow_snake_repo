
draw_self();

clr_grid = make_color_rgb(10,10,10);

for (i = 0; i < rmh; i++)
{
	for (j = 0; j < rmw; j++)
	{
		var ix = i+1;
		var jx = j+1;
		draw_rectangle_color(rmu*j+gofs,rmu*i+gofs, rmu*jx-gofs,rmu*ix-gofs, clr_grid,clr_grid,clr_grid,clr_grid, false);	
	}
}