{
    "id": "6ee9caf9-38d2-4fdc-a6c4-bc9ae19594aa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_snake_head",
    "eventList": [
        {
            "id": "038305d7-4c19-4df5-83a0-3a929e44c900",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6ee9caf9-38d2-4fdc-a6c4-bc9ae19594aa"
        },
        {
            "id": "4419980c-d68e-469c-8385-150bd2fcb0f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6ee9caf9-38d2-4fdc-a6c4-bc9ae19594aa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bb461a8b-f659-4d3d-acac-6731650fc8a5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ae499529-4ebc-4f9d-84f1-766a561e8a9e",
    "visible": true
}