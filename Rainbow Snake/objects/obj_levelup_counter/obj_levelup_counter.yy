{
    "id": "207a4f6a-a590-4114-9eaa-56f90d5b655b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_levelup_counter",
    "eventList": [
        {
            "id": "af75a53d-6625-400a-a809-356adef2836a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "207a4f6a-a590-4114-9eaa-56f90d5b655b"
        },
        {
            "id": "d8932cf4-d1d9-4dd0-83ea-978242586c43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "207a4f6a-a590-4114-9eaa-56f90d5b655b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cc15df7d-158d-45a9-8415-61003b8dcded",
    "visible": true
}