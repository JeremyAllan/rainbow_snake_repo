{
    "id": "76902b89-d273-4ff0-9068-ae77181c003b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "food",
    "eventList": [
        {
            "id": "6afab64d-9657-4dc0-8399-f605881fb98b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "76902b89-d273-4ff0-9068-ae77181c003b"
        },
        {
            "id": "9a5278d9-19ab-466b-b911-8482a3b5422d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "bb461a8b-f659-4d3d-acac-6731650fc8a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "76902b89-d273-4ff0-9068-ae77181c003b"
        },
        {
            "id": "6a20de1c-c7b3-4f6c-81bf-061523e89a80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "76902b89-d273-4ff0-9068-ae77181c003b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1d1794e6-1f5b-4f8e-9407-2d9013f0ccf2",
    "visible": true
}