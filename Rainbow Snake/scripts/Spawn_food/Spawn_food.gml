/// Spawn_food(x,y,rw,rh,layer) 
/// @description Spawn_food(x,y,rw,rh,layer) 
/// @param x 
/// @param y 
/// @param rw 
/// @param rh 
/// @param layer 
 
fx = argument0; 
fy = argument1; 

if (place_empty(fx, fy) == false) || (x >= room_width) || (y >= room_height)
{ 
	var fx = (irandom_range(0, argument2/unit)) * unit; 
	var fy = (irandom_range(0, argument3/unit)) * unit;   
}
else
{
	instance_create_layer(fx, fy, argument4, food); 
}