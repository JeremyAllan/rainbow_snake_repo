/// Colour_picker(clr_for, clr_ar_len)
/// @description Colour_picker(clr_for, clr_ar_len)
/// @param clr_for
/// @param clr_ar_len

var cf = argument0;
var cal = argument1;
clr_new = cal;

if (cf <= 0) {
	clr_new = cal-1;	
}
else  {
	clr_new = cf--;
}

return clr_new;


//var clr_for = argument0; // if the colour in front is = or less than 0
//var ca

//clr_new = 0;

//if (clr_for <= 0)
//{
//	clr_new = array_length_1d(argument1-1);
//}
//else
//{
//	clr_new = clr_for--;
//}

//return clr_new;