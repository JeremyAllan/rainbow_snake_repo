// draw grid script

grid_count_x = room_width / 10;
grid_count_y = room_height / 10;
grid_count_t = grid_count_x * grid_count_y;
// should be 32 x 64 to start

gofs = 2;

for ( i = 0; i < grid_count_t; i++) // in total 
{
	for (j = 0; j < grid_count_x; j++) // going down the column (2)
	{
		for (k = 0; k > grid_count_x; k++) // going down the row (1)
		{
			var nk = k + 1;
			var nj = j + 1;
			draw_rectangle_color(k+gofs, j+gofs, nk-gofs, nj+gofs, c_gray, c_gray, c_gray, c_gray, false);
		}
	}
}