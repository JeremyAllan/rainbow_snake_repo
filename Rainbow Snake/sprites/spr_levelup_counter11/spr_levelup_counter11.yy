{
    "id": "cc15df7d-158d-45a9-8415-61003b8dcded",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_levelup_counter11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 10,
    "bbox_right": 629,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bae0cf9c-246c-4a70-853a-a97248fbd572",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc15df7d-158d-45a9-8415-61003b8dcded",
            "compositeImage": {
                "id": "0d83902d-e5bc-4cf0-b311-dd31c73d1809",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bae0cf9c-246c-4a70-853a-a97248fbd572",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ca168c5-b7a2-4424-b438-898c2d06012f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bae0cf9c-246c-4a70-853a-a97248fbd572",
                    "LayerId": "9312faf7-dd77-404d-993a-ced7fc7bbae3"
                }
            ]
        },
        {
            "id": "6b181535-ec5d-47ac-9462-47518385b208",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc15df7d-158d-45a9-8415-61003b8dcded",
            "compositeImage": {
                "id": "2c5e58c7-5904-4905-b787-8043e3899114",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b181535-ec5d-47ac-9462-47518385b208",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8b356ee-3215-45f6-a5e2-6deaf15cc205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b181535-ec5d-47ac-9462-47518385b208",
                    "LayerId": "9312faf7-dd77-404d-993a-ced7fc7bbae3"
                }
            ]
        },
        {
            "id": "a814c7d7-dc34-44d7-adcf-54b06a0a03c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc15df7d-158d-45a9-8415-61003b8dcded",
            "compositeImage": {
                "id": "f30e2d85-1adf-4344-806b-c68967fd6101",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a814c7d7-dc34-44d7-adcf-54b06a0a03c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bded5fc6-ebce-41ad-83c3-b93482ea6b03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a814c7d7-dc34-44d7-adcf-54b06a0a03c2",
                    "LayerId": "9312faf7-dd77-404d-993a-ced7fc7bbae3"
                }
            ]
        },
        {
            "id": "ffd18a38-0d4a-4b54-b858-1140a78aac53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc15df7d-158d-45a9-8415-61003b8dcded",
            "compositeImage": {
                "id": "09afd9b3-04a5-48a2-b542-cb834641d557",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffd18a38-0d4a-4b54-b858-1140a78aac53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f8812c7-9ce4-4660-89d2-ff9a46978040",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffd18a38-0d4a-4b54-b858-1140a78aac53",
                    "LayerId": "9312faf7-dd77-404d-993a-ced7fc7bbae3"
                }
            ]
        },
        {
            "id": "19db4297-99c6-40ef-a34f-cfe82b112aab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc15df7d-158d-45a9-8415-61003b8dcded",
            "compositeImage": {
                "id": "6a05a22a-96c7-4a63-b507-e303c3508622",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19db4297-99c6-40ef-a34f-cfe82b112aab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdcbfaa8-8a3a-4096-b398-decd4d8d6f64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19db4297-99c6-40ef-a34f-cfe82b112aab",
                    "LayerId": "9312faf7-dd77-404d-993a-ced7fc7bbae3"
                }
            ]
        },
        {
            "id": "6bbd6851-0ddc-428d-aec8-e79fac486d7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc15df7d-158d-45a9-8415-61003b8dcded",
            "compositeImage": {
                "id": "2e7a36e2-befa-46f5-8eef-96acd276c80b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bbd6851-0ddc-428d-aec8-e79fac486d7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da2b016c-0a88-456d-bdf9-1470588d8bed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bbd6851-0ddc-428d-aec8-e79fac486d7d",
                    "LayerId": "9312faf7-dd77-404d-993a-ced7fc7bbae3"
                }
            ]
        },
        {
            "id": "8f2ec36a-4c6b-4e39-a87d-06f86e77558d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc15df7d-158d-45a9-8415-61003b8dcded",
            "compositeImage": {
                "id": "6ec7b66a-c5bf-4c35-ad15-be8485583acd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f2ec36a-4c6b-4e39-a87d-06f86e77558d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7749da28-dff5-4f47-a015-21359a09f52c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f2ec36a-4c6b-4e39-a87d-06f86e77558d",
                    "LayerId": "9312faf7-dd77-404d-993a-ced7fc7bbae3"
                }
            ]
        }
    ],
    "gridX": 10,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "9312faf7-dd77-404d-993a-ced7fc7bbae3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc15df7d-158d-45a9-8415-61003b8dcded",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 31
}