{
    "id": "407ce3aa-2d2a-41e2-ad1a-71fa16d59822",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "982ba6c5-bf1e-4ffb-93cf-216356b968fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "407ce3aa-2d2a-41e2-ad1a-71fa16d59822",
            "compositeImage": {
                "id": "f18c3922-364e-4dbe-8118-edb4ebd0819c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "982ba6c5-bf1e-4ffb-93cf-216356b968fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23b391d6-4cde-4cb5-bbdb-d48580a85f11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "982ba6c5-bf1e-4ffb-93cf-216356b968fb",
                    "LayerId": "ae650a6f-fd60-4ea0-aa54-748e2d1c276c"
                }
            ]
        },
        {
            "id": "e11d7bbb-de6a-4330-ba57-56fc92bcb798",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "407ce3aa-2d2a-41e2-ad1a-71fa16d59822",
            "compositeImage": {
                "id": "d4638c01-a167-46c5-b997-f2fea4156db5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e11d7bbb-de6a-4330-ba57-56fc92bcb798",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e023574-35e0-420d-8883-b51ec03b128a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e11d7bbb-de6a-4330-ba57-56fc92bcb798",
                    "LayerId": "ae650a6f-fd60-4ea0-aa54-748e2d1c276c"
                }
            ]
        },
        {
            "id": "b7f07249-89f3-419b-9bdf-715fd671561a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "407ce3aa-2d2a-41e2-ad1a-71fa16d59822",
            "compositeImage": {
                "id": "ca288e3e-3d96-4bb4-b2c2-8502b601ce11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7f07249-89f3-419b-9bdf-715fd671561a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "317c47a8-b685-4f44-a846-c7aa17657524",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7f07249-89f3-419b-9bdf-715fd671561a",
                    "LayerId": "ae650a6f-fd60-4ea0-aa54-748e2d1c276c"
                }
            ]
        },
        {
            "id": "ab2cfae0-dcc8-4d95-9121-214340782143",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "407ce3aa-2d2a-41e2-ad1a-71fa16d59822",
            "compositeImage": {
                "id": "c856660f-09fb-469c-a343-07540d3cb24b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab2cfae0-dcc8-4d95-9121-214340782143",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "049938b6-c18c-42ac-af76-c0ad16490cbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab2cfae0-dcc8-4d95-9121-214340782143",
                    "LayerId": "ae650a6f-fd60-4ea0-aa54-748e2d1c276c"
                }
            ]
        },
        {
            "id": "0cb3a194-37b5-435b-a4ab-801d389b361d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "407ce3aa-2d2a-41e2-ad1a-71fa16d59822",
            "compositeImage": {
                "id": "bcb10371-1b91-49bb-94c2-147ed17c4b44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cb3a194-37b5-435b-a4ab-801d389b361d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7077af66-c2d8-42e9-bfe4-9b9923fca226",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cb3a194-37b5-435b-a4ab-801d389b361d",
                    "LayerId": "ae650a6f-fd60-4ea0-aa54-748e2d1c276c"
                }
            ]
        },
        {
            "id": "f3b80210-d6be-4508-9cfb-a9c5626ddb39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "407ce3aa-2d2a-41e2-ad1a-71fa16d59822",
            "compositeImage": {
                "id": "4e14a58c-5f61-4004-9e9f-654f51fc9708",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3b80210-d6be-4508-9cfb-a9c5626ddb39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbdfb37b-dba0-407d-a210-f6ca306d01ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3b80210-d6be-4508-9cfb-a9c5626ddb39",
                    "LayerId": "ae650a6f-fd60-4ea0-aa54-748e2d1c276c"
                }
            ]
        },
        {
            "id": "901271f5-11b0-4c92-943c-f935466dfcc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "407ce3aa-2d2a-41e2-ad1a-71fa16d59822",
            "compositeImage": {
                "id": "e330945f-b4bf-493c-a4da-efc41a75fd56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "901271f5-11b0-4c92-943c-f935466dfcc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df35f6d1-d623-43e2-b86e-dc7328a252a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "901271f5-11b0-4c92-943c-f935466dfcc4",
                    "LayerId": "ae650a6f-fd60-4ea0-aa54-748e2d1c276c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ae650a6f-fd60-4ea0-aa54-748e2d1c276c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "407ce3aa-2d2a-41e2-ad1a-71fa16d59822",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}