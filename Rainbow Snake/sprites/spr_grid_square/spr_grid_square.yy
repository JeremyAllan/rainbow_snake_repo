{
    "id": "07b87de7-0886-45f2-b26b-126a8e39d629",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grid_square",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aae3021c-4017-4014-bc39-25b4f80f4c44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07b87de7-0886-45f2-b26b-126a8e39d629",
            "compositeImage": {
                "id": "aeb56623-bd36-40be-bc94-dedf570b53af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aae3021c-4017-4014-bc39-25b4f80f4c44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "493dee7c-156b-49a2-8264-d72560b52e52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aae3021c-4017-4014-bc39-25b4f80f4c44",
                    "LayerId": "62318357-bee2-42fe-8c6b-2773f89f1f12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "62318357-bee2-42fe-8c6b-2773f89f1f12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07b87de7-0886-45f2-b26b-126a8e39d629",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}