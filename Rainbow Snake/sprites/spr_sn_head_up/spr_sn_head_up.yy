{
    "id": "abfb5ec0-7d23-4f5f-b1a6-2229544141f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sn_head_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f72fb794-20d4-4bdd-8b98-aa5cd95ca95a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abfb5ec0-7d23-4f5f-b1a6-2229544141f3",
            "compositeImage": {
                "id": "f5c14510-bddc-4cfc-9ed9-d3e256d481d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f72fb794-20d4-4bdd-8b98-aa5cd95ca95a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8a79cdf-26bb-477d-8527-55b8ecad61c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f72fb794-20d4-4bdd-8b98-aa5cd95ca95a",
                    "LayerId": "7bd04cb6-b831-4728-911b-8492348ec39f"
                }
            ]
        },
        {
            "id": "5cf50d01-de3e-44b6-97ee-e37f4fda25bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abfb5ec0-7d23-4f5f-b1a6-2229544141f3",
            "compositeImage": {
                "id": "7982674e-4c2a-4812-b3ef-b1ac30444498",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cf50d01-de3e-44b6-97ee-e37f4fda25bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76768682-4b95-433d-bd75-43f1b7aa0b44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cf50d01-de3e-44b6-97ee-e37f4fda25bc",
                    "LayerId": "7bd04cb6-b831-4728-911b-8492348ec39f"
                }
            ]
        },
        {
            "id": "1996013e-5360-4e8a-896c-d13fe2dee413",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abfb5ec0-7d23-4f5f-b1a6-2229544141f3",
            "compositeImage": {
                "id": "e50ba912-a8f6-4fdf-b454-61e0d1105bb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1996013e-5360-4e8a-896c-d13fe2dee413",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55c408dd-9871-4985-850f-1eef5e99ff53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1996013e-5360-4e8a-896c-d13fe2dee413",
                    "LayerId": "7bd04cb6-b831-4728-911b-8492348ec39f"
                }
            ]
        },
        {
            "id": "86ba49eb-f330-4840-baaa-f7dce696114e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abfb5ec0-7d23-4f5f-b1a6-2229544141f3",
            "compositeImage": {
                "id": "ad0d8c96-5638-4ff8-a47c-a1bf34d0f425",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86ba49eb-f330-4840-baaa-f7dce696114e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34df7f81-fc69-4296-9c7b-dcd862fd8a11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86ba49eb-f330-4840-baaa-f7dce696114e",
                    "LayerId": "7bd04cb6-b831-4728-911b-8492348ec39f"
                }
            ]
        },
        {
            "id": "1e646b8e-88f7-4b5a-ad0b-de0ffdc158d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abfb5ec0-7d23-4f5f-b1a6-2229544141f3",
            "compositeImage": {
                "id": "5d8edd05-36b2-42ac-ab45-5a759b13d17f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e646b8e-88f7-4b5a-ad0b-de0ffdc158d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7972af8-8af2-4de3-83c8-4e58aeb5bfb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e646b8e-88f7-4b5a-ad0b-de0ffdc158d0",
                    "LayerId": "7bd04cb6-b831-4728-911b-8492348ec39f"
                }
            ]
        },
        {
            "id": "ca793e75-7b05-4e30-bb82-e2bd4de78e25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abfb5ec0-7d23-4f5f-b1a6-2229544141f3",
            "compositeImage": {
                "id": "6d06c766-004c-439e-a40d-9d216a9bac70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca793e75-7b05-4e30-bb82-e2bd4de78e25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3902b6d3-5e68-4d0d-97c5-da05a850c845",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca793e75-7b05-4e30-bb82-e2bd4de78e25",
                    "LayerId": "7bd04cb6-b831-4728-911b-8492348ec39f"
                }
            ]
        },
        {
            "id": "6133b008-e6c7-42ac-937c-43d2ee6ca68d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abfb5ec0-7d23-4f5f-b1a6-2229544141f3",
            "compositeImage": {
                "id": "8c0320ac-22f4-476d-86b4-71b69463955e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6133b008-e6c7-42ac-937c-43d2ee6ca68d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96ae7f6f-6668-45d0-8095-28f3d01f7d26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6133b008-e6c7-42ac-937c-43d2ee6ca68d",
                    "LayerId": "7bd04cb6-b831-4728-911b-8492348ec39f"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 32,
    "layers": [
        {
            "id": "7bd04cb6-b831-4728-911b-8492348ec39f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abfb5ec0-7d23-4f5f-b1a6-2229544141f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}