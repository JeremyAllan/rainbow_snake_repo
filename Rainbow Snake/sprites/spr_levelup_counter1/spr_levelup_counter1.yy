{
    "id": "c9643a4f-118c-4b1a-a738-a0198fa0b02b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_levelup_counter1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 10,
    "bbox_right": 629,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd28d9ea-3758-489d-abf7-73c5b507aa13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9643a4f-118c-4b1a-a738-a0198fa0b02b",
            "compositeImage": {
                "id": "ccc60b53-578d-416c-8fcd-a590b14e2311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd28d9ea-3758-489d-abf7-73c5b507aa13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1930d025-0421-49c1-82ad-7af2b5b1bc9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd28d9ea-3758-489d-abf7-73c5b507aa13",
                    "LayerId": "f748a440-1d76-49c2-9c9e-f94e1a3d3d2a"
                }
            ]
        },
        {
            "id": "aee2537e-7bb1-4fad-8239-27d7030e0712",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9643a4f-118c-4b1a-a738-a0198fa0b02b",
            "compositeImage": {
                "id": "022b4428-2b35-44f6-8256-6e49fe6fae85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aee2537e-7bb1-4fad-8239-27d7030e0712",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bafb111-fce0-4947-9cce-e1ef91464b03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aee2537e-7bb1-4fad-8239-27d7030e0712",
                    "LayerId": "f748a440-1d76-49c2-9c9e-f94e1a3d3d2a"
                }
            ]
        },
        {
            "id": "199461b7-306e-44ad-afa6-010bc17a8b44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9643a4f-118c-4b1a-a738-a0198fa0b02b",
            "compositeImage": {
                "id": "c8378436-26cf-4936-bf4d-4f9720aac051",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "199461b7-306e-44ad-afa6-010bc17a8b44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1126dea7-cac5-4750-9960-71cadb4e95e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "199461b7-306e-44ad-afa6-010bc17a8b44",
                    "LayerId": "f748a440-1d76-49c2-9c9e-f94e1a3d3d2a"
                }
            ]
        },
        {
            "id": "7c8c2234-c90e-4e9f-8d9f-7c3175f4c404",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9643a4f-118c-4b1a-a738-a0198fa0b02b",
            "compositeImage": {
                "id": "c98fd5d4-c35b-4065-abb7-bad4b6f06932",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c8c2234-c90e-4e9f-8d9f-7c3175f4c404",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02178579-f885-4a13-ae81-ebd3283ab878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c8c2234-c90e-4e9f-8d9f-7c3175f4c404",
                    "LayerId": "f748a440-1d76-49c2-9c9e-f94e1a3d3d2a"
                }
            ]
        },
        {
            "id": "90b0c675-2e66-4252-a6de-1f6d90ff2949",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9643a4f-118c-4b1a-a738-a0198fa0b02b",
            "compositeImage": {
                "id": "c3a26248-dc03-4a2c-bb53-d9c771c591b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90b0c675-2e66-4252-a6de-1f6d90ff2949",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f15f76c-e66f-4dd8-8c86-b28934385b55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90b0c675-2e66-4252-a6de-1f6d90ff2949",
                    "LayerId": "f748a440-1d76-49c2-9c9e-f94e1a3d3d2a"
                }
            ]
        },
        {
            "id": "55927aa9-e909-4baa-bbbb-ec43d25fcfc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9643a4f-118c-4b1a-a738-a0198fa0b02b",
            "compositeImage": {
                "id": "e9b90ff6-0683-471d-8e03-8917e19a0abb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55927aa9-e909-4baa-bbbb-ec43d25fcfc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a699f8fb-d74b-478b-ae6b-94e320b653e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55927aa9-e909-4baa-bbbb-ec43d25fcfc3",
                    "LayerId": "f748a440-1d76-49c2-9c9e-f94e1a3d3d2a"
                }
            ]
        },
        {
            "id": "0a783d91-b02e-4f9d-b240-7545cad6b400",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9643a4f-118c-4b1a-a738-a0198fa0b02b",
            "compositeImage": {
                "id": "5dc54523-e7b5-4d11-9f8d-231f7c4a06f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a783d91-b02e-4f9d-b240-7545cad6b400",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc5782d1-71b4-4f5d-bf10-b52b6192e303",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a783d91-b02e-4f9d-b240-7545cad6b400",
                    "LayerId": "f748a440-1d76-49c2-9c9e-f94e1a3d3d2a"
                }
            ]
        }
    ],
    "gridX": 10,
    "gridY": 16,
    "height": 32,
    "layers": [
        {
            "id": "f748a440-1d76-49c2-9c9e-f94e1a3d3d2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9643a4f-118c-4b1a-a738-a0198fa0b02b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 31
}