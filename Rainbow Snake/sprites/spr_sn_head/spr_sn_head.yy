{
    "id": "8115eb04-6cd7-40a5-9aec-6623e96b2207",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sn_head",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0acef0e4-db2f-4ba4-b080-fcd12b9745db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8115eb04-6cd7-40a5-9aec-6623e96b2207",
            "compositeImage": {
                "id": "cf3daebf-3517-4d4b-84f2-dfd66bf474bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0acef0e4-db2f-4ba4-b080-fcd12b9745db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1397ff77-3f87-46da-b3fb-03ed7a442aec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0acef0e4-db2f-4ba4-b080-fcd12b9745db",
                    "LayerId": "e6425ec9-09ad-4914-acdc-f1645e393ce7"
                }
            ]
        },
        {
            "id": "88a8a543-a699-4f6f-83e7-33227a835edc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8115eb04-6cd7-40a5-9aec-6623e96b2207",
            "compositeImage": {
                "id": "293ef54c-dbc8-443d-98bb-4b1fd781b5b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88a8a543-a699-4f6f-83e7-33227a835edc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd6978a5-b581-456d-b89d-ea809292d988",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88a8a543-a699-4f6f-83e7-33227a835edc",
                    "LayerId": "e6425ec9-09ad-4914-acdc-f1645e393ce7"
                }
            ]
        },
        {
            "id": "14ddd9a2-80bc-41ff-a248-8db52e88d7de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8115eb04-6cd7-40a5-9aec-6623e96b2207",
            "compositeImage": {
                "id": "558dfc67-5e01-4558-b2ec-eaf8e2a91843",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14ddd9a2-80bc-41ff-a248-8db52e88d7de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10a11a6a-709c-4352-8de3-666260a55bf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14ddd9a2-80bc-41ff-a248-8db52e88d7de",
                    "LayerId": "e6425ec9-09ad-4914-acdc-f1645e393ce7"
                }
            ]
        },
        {
            "id": "511ba54d-7bf2-4daa-958a-4c8f3a38ba44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8115eb04-6cd7-40a5-9aec-6623e96b2207",
            "compositeImage": {
                "id": "587deaab-ecb5-4d69-9413-6ce2d2366237",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "511ba54d-7bf2-4daa-958a-4c8f3a38ba44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4f3380b-fcc3-47af-9323-16ba1f9fa022",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "511ba54d-7bf2-4daa-958a-4c8f3a38ba44",
                    "LayerId": "e6425ec9-09ad-4914-acdc-f1645e393ce7"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 32,
    "layers": [
        {
            "id": "e6425ec9-09ad-4914-acdc-f1645e393ce7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8115eb04-6cd7-40a5-9aec-6623e96b2207",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}