{
    "id": "a87e5b72-aaf9-4dd9-8243-b7157af26da8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sn_head_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31c65029-db08-4872-ab9a-f33f92ee3d1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a87e5b72-aaf9-4dd9-8243-b7157af26da8",
            "compositeImage": {
                "id": "e7c1b6be-d40b-4be6-9eed-810222569c82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31c65029-db08-4872-ab9a-f33f92ee3d1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f4961c1-9a91-401e-b226-1218814e5267",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31c65029-db08-4872-ab9a-f33f92ee3d1f",
                    "LayerId": "951a4cc0-7bf5-48e0-9e99-b1cf8d72bd8a"
                }
            ]
        },
        {
            "id": "2f4053f6-45d5-486f-85fe-9519ee06bc08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a87e5b72-aaf9-4dd9-8243-b7157af26da8",
            "compositeImage": {
                "id": "097aa75a-8d02-4c43-9e96-314ef4da7300",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f4053f6-45d5-486f-85fe-9519ee06bc08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a22e0bdf-4acd-4fc3-a1b2-57d40b447d61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f4053f6-45d5-486f-85fe-9519ee06bc08",
                    "LayerId": "951a4cc0-7bf5-48e0-9e99-b1cf8d72bd8a"
                }
            ]
        },
        {
            "id": "628df831-ed9c-49b5-b3d9-82c7fc176b1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a87e5b72-aaf9-4dd9-8243-b7157af26da8",
            "compositeImage": {
                "id": "9edc46b7-1fce-4ed6-a6e4-12a4a7b47b47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "628df831-ed9c-49b5-b3d9-82c7fc176b1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d4eb557-6e5a-42b1-8e8c-8e8fe47e04d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "628df831-ed9c-49b5-b3d9-82c7fc176b1a",
                    "LayerId": "951a4cc0-7bf5-48e0-9e99-b1cf8d72bd8a"
                }
            ]
        },
        {
            "id": "c3eca7f9-5fce-4286-9043-34eb7c03d3f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a87e5b72-aaf9-4dd9-8243-b7157af26da8",
            "compositeImage": {
                "id": "46127967-3f80-427b-a594-69a6b63378af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3eca7f9-5fce-4286-9043-34eb7c03d3f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "860c4472-c2df-47cf-aa4d-67dc09b341ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3eca7f9-5fce-4286-9043-34eb7c03d3f3",
                    "LayerId": "951a4cc0-7bf5-48e0-9e99-b1cf8d72bd8a"
                }
            ]
        },
        {
            "id": "218480e3-5e1c-445d-ba21-72311c38f9c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a87e5b72-aaf9-4dd9-8243-b7157af26da8",
            "compositeImage": {
                "id": "8507b70f-d197-4979-bd38-4f43ad7ffb4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "218480e3-5e1c-445d-ba21-72311c38f9c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed8f0539-eb17-4c29-9014-826a26cb9289",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "218480e3-5e1c-445d-ba21-72311c38f9c3",
                    "LayerId": "951a4cc0-7bf5-48e0-9e99-b1cf8d72bd8a"
                }
            ]
        },
        {
            "id": "06422546-ccda-4a03-8a01-5eb180ad9af4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a87e5b72-aaf9-4dd9-8243-b7157af26da8",
            "compositeImage": {
                "id": "7e18311d-270b-4d2c-b49e-12ca35e7ad79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06422546-ccda-4a03-8a01-5eb180ad9af4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c0dd083-1873-416a-8960-88d4663c3f54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06422546-ccda-4a03-8a01-5eb180ad9af4",
                    "LayerId": "951a4cc0-7bf5-48e0-9e99-b1cf8d72bd8a"
                }
            ]
        },
        {
            "id": "a55993fe-6534-47d1-804c-11186ff8c229",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a87e5b72-aaf9-4dd9-8243-b7157af26da8",
            "compositeImage": {
                "id": "3c3a4f09-8898-4f00-a6f9-b2f2b28c990f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a55993fe-6534-47d1-804c-11186ff8c229",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f017942-5fcf-4fd4-8b07-b06de91bbc68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a55993fe-6534-47d1-804c-11186ff8c229",
                    "LayerId": "951a4cc0-7bf5-48e0-9e99-b1cf8d72bd8a"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 32,
    "layers": [
        {
            "id": "951a4cc0-7bf5-48e0-9e99-b1cf8d72bd8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a87e5b72-aaf9-4dd9-8243-b7157af26da8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}