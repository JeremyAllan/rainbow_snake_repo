{
    "id": "ae499529-4ebc-4f9d-84f1-766a561e8a9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sn_head_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d3107d1-b1b1-4f27-ad64-970cd2a22f87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae499529-4ebc-4f9d-84f1-766a561e8a9e",
            "compositeImage": {
                "id": "0ae3fe9d-70f5-4493-8bd1-f57230656bb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d3107d1-b1b1-4f27-ad64-970cd2a22f87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30c4aa02-1c1c-4b2f-82b4-bc020c521e40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d3107d1-b1b1-4f27-ad64-970cd2a22f87",
                    "LayerId": "8383bb5d-e021-4549-b19a-afa3e06fcdf2"
                }
            ]
        },
        {
            "id": "d910b5e5-9070-4ee7-92a6-997386eb238c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae499529-4ebc-4f9d-84f1-766a561e8a9e",
            "compositeImage": {
                "id": "6f61c435-7413-4ab4-89d0-506cd262761d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d910b5e5-9070-4ee7-92a6-997386eb238c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b7571b7-8ae4-49e6-8840-6bcbba8b7a1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d910b5e5-9070-4ee7-92a6-997386eb238c",
                    "LayerId": "8383bb5d-e021-4549-b19a-afa3e06fcdf2"
                }
            ]
        },
        {
            "id": "19b8878a-2803-409f-abc8-ada5600e1455",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae499529-4ebc-4f9d-84f1-766a561e8a9e",
            "compositeImage": {
                "id": "c832f5de-7ac2-46a1-985f-8adf8e748b37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19b8878a-2803-409f-abc8-ada5600e1455",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb11ce5b-77bd-45c8-90de-bc1a30ee595e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19b8878a-2803-409f-abc8-ada5600e1455",
                    "LayerId": "8383bb5d-e021-4549-b19a-afa3e06fcdf2"
                }
            ]
        },
        {
            "id": "5cf236f3-d8fe-4cbf-9211-69d9c5649f8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae499529-4ebc-4f9d-84f1-766a561e8a9e",
            "compositeImage": {
                "id": "1cf4e837-e71a-4489-aabc-62d8df96dbc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cf236f3-d8fe-4cbf-9211-69d9c5649f8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73d733b2-2db8-487e-ba4d-bfba2d7773d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cf236f3-d8fe-4cbf-9211-69d9c5649f8e",
                    "LayerId": "8383bb5d-e021-4549-b19a-afa3e06fcdf2"
                }
            ]
        },
        {
            "id": "a3a766b4-b134-4ae9-9fd8-d11d8310926b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae499529-4ebc-4f9d-84f1-766a561e8a9e",
            "compositeImage": {
                "id": "8b7baf76-a222-4a7e-9167-778bc073dfef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3a766b4-b134-4ae9-9fd8-d11d8310926b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bac6164-89ab-474d-97e8-a0322e6a6188",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3a766b4-b134-4ae9-9fd8-d11d8310926b",
                    "LayerId": "8383bb5d-e021-4549-b19a-afa3e06fcdf2"
                }
            ]
        },
        {
            "id": "f7061d86-e893-44d2-9b57-853292db08d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae499529-4ebc-4f9d-84f1-766a561e8a9e",
            "compositeImage": {
                "id": "31f00035-3aca-4131-8c65-1b2975b42639",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7061d86-e893-44d2-9b57-853292db08d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fd13b4f-6ef7-4485-95ad-85f65129e387",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7061d86-e893-44d2-9b57-853292db08d1",
                    "LayerId": "8383bb5d-e021-4549-b19a-afa3e06fcdf2"
                }
            ]
        },
        {
            "id": "1650a4b1-94db-4148-8f93-69e717f0dc28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae499529-4ebc-4f9d-84f1-766a561e8a9e",
            "compositeImage": {
                "id": "5cb39cf0-6dba-49b0-9156-921e01aa8be4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1650a4b1-94db-4148-8f93-69e717f0dc28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5747be98-d24e-41ab-b1d9-1f4e16aa4b23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1650a4b1-94db-4148-8f93-69e717f0dc28",
                    "LayerId": "8383bb5d-e021-4549-b19a-afa3e06fcdf2"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 32,
    "layers": [
        {
            "id": "8383bb5d-e021-4549-b19a-afa3e06fcdf2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae499529-4ebc-4f9d-84f1-766a561e8a9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}