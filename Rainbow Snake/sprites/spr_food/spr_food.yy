{
    "id": "1d1794e6-1f5b-4f8e-9407-2d9013f0ccf2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_food",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8bc5b45f-f25d-4453-b944-1e2e97550a1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d1794e6-1f5b-4f8e-9407-2d9013f0ccf2",
            "compositeImage": {
                "id": "0c8077ce-3fe2-4ccc-b9c4-01240d198608",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bc5b45f-f25d-4453-b944-1e2e97550a1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e23be528-c00e-41fb-adbb-e73945406fbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bc5b45f-f25d-4453-b944-1e2e97550a1c",
                    "LayerId": "6d38af1a-641f-4943-880a-920594424ccd"
                },
                {
                    "id": "9bfeced5-9b2d-4caf-878b-3828764abe26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bc5b45f-f25d-4453-b944-1e2e97550a1c",
                    "LayerId": "87c6d231-a83b-4bda-ac9b-4ea5b2e3152a"
                }
            ]
        },
        {
            "id": "b3be6b2b-d5d1-41b8-af9c-09204d48d2fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d1794e6-1f5b-4f8e-9407-2d9013f0ccf2",
            "compositeImage": {
                "id": "1aaa4a13-2c98-403b-8b25-858ebb36aec2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3be6b2b-d5d1-41b8-af9c-09204d48d2fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb73a1c1-9238-41fe-bff9-4159c1eee858",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3be6b2b-d5d1-41b8-af9c-09204d48d2fe",
                    "LayerId": "87c6d231-a83b-4bda-ac9b-4ea5b2e3152a"
                },
                {
                    "id": "93537ec0-bef4-4a54-ac04-39230377f0de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3be6b2b-d5d1-41b8-af9c-09204d48d2fe",
                    "LayerId": "6d38af1a-641f-4943-880a-920594424ccd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "87c6d231-a83b-4bda-ac9b-4ea5b2e3152a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d1794e6-1f5b-4f8e-9407-2d9013f0ccf2",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "6d38af1a-641f-4943-880a-920594424ccd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d1794e6-1f5b-4f8e-9407-2d9013f0ccf2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}