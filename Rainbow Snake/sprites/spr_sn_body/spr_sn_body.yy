{
    "id": "e05d0bb6-9604-42f6-8d30-b0b7e6f988eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sn_body",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ab9979d1-4172-4ca9-a2f8-4598f7a3567b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e05d0bb6-9604-42f6-8d30-b0b7e6f988eb",
            "compositeImage": {
                "id": "f9f1b5fb-d49b-429c-b7b9-0c797544054b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab9979d1-4172-4ca9-a2f8-4598f7a3567b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ee3c099-ee7c-494e-a96b-1cdd4b2b64e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab9979d1-4172-4ca9-a2f8-4598f7a3567b",
                    "LayerId": "c77456c1-bbaf-4e31-94e8-94049628b27b"
                },
                {
                    "id": "02434585-eeb7-4782-83ed-01338e7d791a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab9979d1-4172-4ca9-a2f8-4598f7a3567b",
                    "LayerId": "501ec3fc-3cb0-4460-b670-f3f5c68a573a"
                }
            ]
        },
        {
            "id": "b2b796b9-1838-4087-ba22-35d301b90bc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e05d0bb6-9604-42f6-8d30-b0b7e6f988eb",
            "compositeImage": {
                "id": "d8793164-ff84-4203-94c2-7414fc2b79cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2b796b9-1838-4087-ba22-35d301b90bc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2c36cb5-e073-4cb2-96cc-451d12154c22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2b796b9-1838-4087-ba22-35d301b90bc3",
                    "LayerId": "c77456c1-bbaf-4e31-94e8-94049628b27b"
                },
                {
                    "id": "863deafa-f1d4-4c96-8a7c-02fcae2be6c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2b796b9-1838-4087-ba22-35d301b90bc3",
                    "LayerId": "501ec3fc-3cb0-4460-b670-f3f5c68a573a"
                }
            ]
        },
        {
            "id": "31e761df-1c1c-479e-b02a-4dec4dbbc1f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e05d0bb6-9604-42f6-8d30-b0b7e6f988eb",
            "compositeImage": {
                "id": "ab2b4e46-7573-4282-8ea1-37863dd9aa1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31e761df-1c1c-479e-b02a-4dec4dbbc1f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e113176e-3615-44a3-b6f4-cac2db1b35af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31e761df-1c1c-479e-b02a-4dec4dbbc1f5",
                    "LayerId": "c77456c1-bbaf-4e31-94e8-94049628b27b"
                },
                {
                    "id": "00ecbe2d-21b1-44c4-8f14-5cf50b847a35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31e761df-1c1c-479e-b02a-4dec4dbbc1f5",
                    "LayerId": "501ec3fc-3cb0-4460-b670-f3f5c68a573a"
                }
            ]
        },
        {
            "id": "e28639d5-b989-41ef-bee8-445a00506c22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e05d0bb6-9604-42f6-8d30-b0b7e6f988eb",
            "compositeImage": {
                "id": "58a8cc5d-a49e-4634-b4a4-dea9dbcb1dd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e28639d5-b989-41ef-bee8-445a00506c22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbaf9fc0-70c9-4505-a292-ac2e7392556f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e28639d5-b989-41ef-bee8-445a00506c22",
                    "LayerId": "c77456c1-bbaf-4e31-94e8-94049628b27b"
                },
                {
                    "id": "bc81748b-030e-446d-8cb8-de19e4d89a09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e28639d5-b989-41ef-bee8-445a00506c22",
                    "LayerId": "501ec3fc-3cb0-4460-b670-f3f5c68a573a"
                }
            ]
        },
        {
            "id": "fb217623-9bba-40a7-bbcc-ff33f67a94c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e05d0bb6-9604-42f6-8d30-b0b7e6f988eb",
            "compositeImage": {
                "id": "692c0f30-4747-4d0a-90af-bc85afb93f1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb217623-9bba-40a7-bbcc-ff33f67a94c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5f5bf3d-2744-4f8d-b2f4-485e31c9a39e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb217623-9bba-40a7-bbcc-ff33f67a94c8",
                    "LayerId": "c77456c1-bbaf-4e31-94e8-94049628b27b"
                },
                {
                    "id": "37ecdb35-a75a-4f0f-8ee6-14c3798cbca4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb217623-9bba-40a7-bbcc-ff33f67a94c8",
                    "LayerId": "501ec3fc-3cb0-4460-b670-f3f5c68a573a"
                }
            ]
        },
        {
            "id": "8fe02007-5b5f-40d1-9443-35c289b53a94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e05d0bb6-9604-42f6-8d30-b0b7e6f988eb",
            "compositeImage": {
                "id": "392f4789-6c80-46df-96c2-d9fbdc38e7a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fe02007-5b5f-40d1-9443-35c289b53a94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca47b3b4-2db4-4d03-ae96-99c32972b3c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fe02007-5b5f-40d1-9443-35c289b53a94",
                    "LayerId": "c77456c1-bbaf-4e31-94e8-94049628b27b"
                },
                {
                    "id": "ed68d38c-aeea-4571-ae84-4299f8b9108b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fe02007-5b5f-40d1-9443-35c289b53a94",
                    "LayerId": "501ec3fc-3cb0-4460-b670-f3f5c68a573a"
                }
            ]
        },
        {
            "id": "940c1f52-194f-4566-b7fb-11a74d8be073",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e05d0bb6-9604-42f6-8d30-b0b7e6f988eb",
            "compositeImage": {
                "id": "64c7ebe8-bbc5-45d6-8fe7-ada590a7ed63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "940c1f52-194f-4566-b7fb-11a74d8be073",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7d6869b-046c-447f-8770-6565aafc70da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "940c1f52-194f-4566-b7fb-11a74d8be073",
                    "LayerId": "c77456c1-bbaf-4e31-94e8-94049628b27b"
                },
                {
                    "id": "c0133a4a-64fc-4fb9-8e43-d09d17e6e300",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "940c1f52-194f-4566-b7fb-11a74d8be073",
                    "LayerId": "501ec3fc-3cb0-4460-b670-f3f5c68a573a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "501ec3fc-3cb0-4460-b670-f3f5c68a573a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e05d0bb6-9604-42f6-8d30-b0b7e6f988eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "c77456c1-bbaf-4e31-94e8-94049628b27b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e05d0bb6-9604-42f6-8d30-b0b7e6f988eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}