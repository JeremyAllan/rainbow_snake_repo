{
    "id": "eec652e1-3bc5-47e1-a936-bfad6a124f41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_snake",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78d6942e-5d36-49cf-b11e-3bfc54096cc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eec652e1-3bc5-47e1-a936-bfad6a124f41",
            "compositeImage": {
                "id": "726b2ea4-eacd-4b85-86fe-3eeed0b883bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78d6942e-5d36-49cf-b11e-3bfc54096cc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "066e5fec-b847-415b-8faa-e6272f69ed2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78d6942e-5d36-49cf-b11e-3bfc54096cc2",
                    "LayerId": "cea74340-853d-480b-8e0c-ebc834ab9e2f"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 32,
    "layers": [
        {
            "id": "cea74340-853d-480b-8e0c-ebc834ab9e2f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eec652e1-3bc5-47e1-a936-bfad6a124f41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": -5,
    "yorig": 16
}