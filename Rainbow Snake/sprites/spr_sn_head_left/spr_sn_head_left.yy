{
    "id": "76c8a31a-6ca9-4338-882d-43a9557c5524",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sn_head_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9620e548-fc60-4fcf-acc1-094a1cb96503",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76c8a31a-6ca9-4338-882d-43a9557c5524",
            "compositeImage": {
                "id": "83ae0e96-6cbb-4906-aa18-50645469831b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9620e548-fc60-4fcf-acc1-094a1cb96503",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13f26245-2bea-4f2d-843c-504fe424f8a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9620e548-fc60-4fcf-acc1-094a1cb96503",
                    "LayerId": "ca4d0016-0190-4380-b934-ad30277cab57"
                }
            ]
        },
        {
            "id": "e2237c61-aba6-40e8-a526-91d8a0c9c932",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76c8a31a-6ca9-4338-882d-43a9557c5524",
            "compositeImage": {
                "id": "86ebb216-ddbe-4a46-be2d-2e6c59e31e7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2237c61-aba6-40e8-a526-91d8a0c9c932",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34e9a6ec-a03d-4450-b033-dad34f7c511b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2237c61-aba6-40e8-a526-91d8a0c9c932",
                    "LayerId": "ca4d0016-0190-4380-b934-ad30277cab57"
                }
            ]
        },
        {
            "id": "86800e49-36f6-4f90-960a-4eb435a6b8a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76c8a31a-6ca9-4338-882d-43a9557c5524",
            "compositeImage": {
                "id": "0375f897-7a4e-46ca-af72-b56bb540a5d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86800e49-36f6-4f90-960a-4eb435a6b8a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c601dbe-e1f9-4d4f-a9e2-c0c64f58d596",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86800e49-36f6-4f90-960a-4eb435a6b8a2",
                    "LayerId": "ca4d0016-0190-4380-b934-ad30277cab57"
                }
            ]
        },
        {
            "id": "00b2b116-def2-459e-989e-ec4aa4bdeb40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76c8a31a-6ca9-4338-882d-43a9557c5524",
            "compositeImage": {
                "id": "fe2543fd-a6dc-48d4-9e94-dc0e56222e62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00b2b116-def2-459e-989e-ec4aa4bdeb40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "075a7bfe-ba4a-41ba-b86b-ed0304d7c3c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00b2b116-def2-459e-989e-ec4aa4bdeb40",
                    "LayerId": "ca4d0016-0190-4380-b934-ad30277cab57"
                }
            ]
        },
        {
            "id": "13e06db5-1769-42aa-9332-b7d8c98baaf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76c8a31a-6ca9-4338-882d-43a9557c5524",
            "compositeImage": {
                "id": "ecce742d-5971-42c4-9f46-294f2137ca78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13e06db5-1769-42aa-9332-b7d8c98baaf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5024475-d6b3-42e7-a2c4-be5bcf99bd1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13e06db5-1769-42aa-9332-b7d8c98baaf7",
                    "LayerId": "ca4d0016-0190-4380-b934-ad30277cab57"
                }
            ]
        },
        {
            "id": "eb53ce70-cea4-4ea2-b225-8f6317830368",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76c8a31a-6ca9-4338-882d-43a9557c5524",
            "compositeImage": {
                "id": "49001223-efd0-49e2-9bb2-3fc68875141c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb53ce70-cea4-4ea2-b225-8f6317830368",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fbccca8-8db4-4150-a258-1e65529131db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb53ce70-cea4-4ea2-b225-8f6317830368",
                    "LayerId": "ca4d0016-0190-4380-b934-ad30277cab57"
                }
            ]
        },
        {
            "id": "079462e2-e388-4e9f-8285-22f53dc9c3c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76c8a31a-6ca9-4338-882d-43a9557c5524",
            "compositeImage": {
                "id": "e1ef302e-5a05-483b-ba4b-fa518becf321",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "079462e2-e388-4e9f-8285-22f53dc9c3c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b476a448-faea-431b-bbcf-bfb978a64775",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "079462e2-e388-4e9f-8285-22f53dc9c3c7",
                    "LayerId": "ca4d0016-0190-4380-b934-ad30277cab57"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 32,
    "layers": [
        {
            "id": "ca4d0016-0190-4380-b934-ad30277cab57",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76c8a31a-6ca9-4338-882d-43a9557c5524",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}