{
    "id": "d72460ff-5f25-438f-837e-f2e556425ad8",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_score",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bahnschrift",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "2f042ee5-aca1-4bd6-823c-acfbc8168247",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 45,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 111,
                "y": 143
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a48c3dcd-7416-4fb2-a5d2-74b810359916",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 45,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 154,
                "y": 143
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "57239bd4-a340-4fd2-a1b0-b61fd1c913f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 45,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 2,
                "y": 143
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3c633b74-4441-4104-993e-1d10b05c0bc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 45,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 318,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "b3be0662-77c4-4a56-807a-5deb4af5dafa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 45,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 108,
                "y": 49
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a8e8e90f-3824-4d7e-937a-56318dd8590c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 45,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "2f9b3be3-3faf-4672-8e1a-e5dace882308",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 45,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "81b11201-80b0-4bb7-83bc-8ae86b911b94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 45,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 140,
                "y": 143
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "878a5574-7dd2-4ed6-8c94-70c694f254d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 45,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 14,
                "y": 143
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a020b4f4-be3a-4b37-a674-55cefe6c93d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 45,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 26,
                "y": 143
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "537ea0c2-d2bd-4228-8c39-83897b927530",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 45,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 357,
                "y": 96
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e850625a-44ea-43ba-a8da-8f2826bc6d4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 38,
                "y": 96
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "af7e0dd0-3621-4984-9d7d-9c1c6215dfd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 45,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 119,
                "y": 143
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d0b6808c-0f81-41b0-a482-a1f8a52e65a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 407,
                "y": 96
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5b61fe8e-62a8-4b35-90f5-740bf516aa26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 45,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 161,
                "y": 143
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c9666dfb-2a0e-4994-b4c9-fa6e4d2cd726",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 45,
                "offset": -1,
                "shift": 14,
                "w": 17,
                "x": 384,
                "y": 49
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9988d33e-c333-4021-93ce-722049e90061",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 92,
                "y": 96
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5bfd1385-0552-413b-a455-e8c9762dde0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 45,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 38,
                "y": 143
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3d00050e-f33b-4437-8b44-a2e21639e97c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 289,
                "y": 49
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "6cdf0d3a-6f09-4963-8ed1-e4e3b47d5888",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 422,
                "y": 49
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e5032203-7516-4137-b5db-f859aadb86a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 45,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 250,
                "y": 49
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "bf88e710-3184-4998-ad1b-0929d1ed9702",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 254,
                "y": 96
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "33867c11-3b1c-4493-bafe-5be7d58fc784",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 45,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 323,
                "y": 96
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "1a88fe7c-fc07-442c-afb4-6e839c36ff78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 164,
                "y": 96
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3575997d-c102-4c7c-a2df-6aca75dacb91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 45,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 365,
                "y": 49
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "46f7f4bd-e765-4487-b012-4aea2ecb5284",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 45,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 340,
                "y": 96
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "9932a283-25f8-4792-891a-fd7243f47b17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 45,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 126,
                "y": 143
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b9ef9e5d-4195-4b67-b6c1-546dfa682e9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 45,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 147,
                "y": 143
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a93231d4-7924-4756-bd58-42f604748352",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 453,
                "y": 96
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b2e2f4a9-e0db-472d-8778-f192af80ee50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 45,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 423,
                "y": 96
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7f3edbb9-1666-418e-8baa-68b86a88f1b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 45,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 438,
                "y": 96
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9efa7f10-3a1b-4d19-a4bd-44d6f27722c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 45,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 374,
                "y": 96
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "be62e654-f8ab-4291-aad0-799c47f00c8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 45,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "1e397bb3-4f47-4b87-a98a-fe61d41e94e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 45,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "69f9266d-a297-47b9-9fd1-7f155c1a69f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 45,
                "offset": 3,
                "shift": 24,
                "w": 19,
                "x": 87,
                "y": 49
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b4374425-95ae-4068-aac3-ae5a155c6c18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 45,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 66,
                "y": 49
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d412afe6-63e7-4cd3-a18b-8da8dda87552",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 45,
                "offset": 3,
                "shift": 24,
                "w": 19,
                "x": 45,
                "y": 49
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b8915a88-f243-422d-b354-2ccf7ca26344",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 45,
                "offset": 3,
                "shift": 22,
                "w": 18,
                "x": 170,
                "y": 49
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "8ccf5e0f-ea41-41bb-adb6-87c0fae501be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 45,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 403,
                "y": 49
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "21a096bf-280a-4859-95ed-09428e5b4346",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 45,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 386,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "284d9092-e016-4d35-9a7d-a564c60fd5f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 45,
                "offset": 3,
                "shift": 25,
                "w": 19,
                "x": 24,
                "y": 49
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ee7a2358-3dbc-4034-8af5-8a0771a95067",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 45,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 168,
                "y": 143
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "dce72527-848b-416d-bec9-95a13ccd856b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 45,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 110,
                "y": 96
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e278458b-5e76-406e-80d0-b5587a9b786a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 45,
                "offset": 3,
                "shift": 24,
                "w": 21,
                "x": 295,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2fdb7aa0-9606-4281-9bee-7c95497cb2b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 45,
                "offset": 3,
                "shift": 21,
                "w": 18,
                "x": 230,
                "y": 49
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "8abd1e46-871d-42af-b950-e74fdaced9b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 45,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a9f06ec0-bd5f-4165-8831-0c1910de943d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 45,
                "offset": 3,
                "shift": 26,
                "w": 20,
                "x": 364,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "83ed2610-af3f-4361-b2fe-19f3f26b06e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 45,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 474,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b51b653d-9470-48b1-b6f9-d279a034bb8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 45,
                "offset": 3,
                "shift": 23,
                "w": 19,
                "x": 129,
                "y": 49
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "88ef84cc-08e6-4135-96fe-cc17abc3fd2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 45,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 248,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4f222d70-e641-4e43-9896-6ff125feeb74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 45,
                "offset": 3,
                "shift": 24,
                "w": 20,
                "x": 408,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "6f71c511-b29f-46f8-890c-0290e851c405",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 45,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 430,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "96241fa7-b660-43cf-aab6-89a02c3cea17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 45,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 452,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "0624d5be-c385-4471-9bf9-83f42ec1020d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 45,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 2,
                "y": 49
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "75f82c02-da2a-4c4e-ae8a-b23290ed0ee9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 45,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ec6b030b-0b9e-4ec7-8775-c3fee672ccd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 45,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2e1cc407-914b-48dc-88ea-d2c7f6282fc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 45,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 272,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "bf5bdcc4-9f00-4903-8c92-532305cbbe9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 45,
                "offset": -1,
                "shift": 19,
                "w": 21,
                "x": 341,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "391bda74-b5cf-44ef-9b8f-f64ea92ccc50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 327,
                "y": 49
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "09e7e522-8c4e-4609-99e5-8244fe1aa8d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 45,
                "offset": 3,
                "shift": 11,
                "w": 8,
                "x": 71,
                "y": 143
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "88f6e07c-d2a9-49dc-8e3b-1581d591d343",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 45,
                "offset": -2,
                "shift": 14,
                "w": 17,
                "x": 270,
                "y": 49
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "c88a0930-ac79-4288-b0e8-aa404fecf126",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 45,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 81,
                "y": 143
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e9da634e-5601-4f39-938e-d374fb018617",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 45,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 289,
                "y": 96
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "538eb1fb-25d2-4172-b660-5a6a1edef630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 45,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 441,
                "y": 49
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a635edbe-a3f0-4dc9-9a54-60e4f036ad33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 45,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 60,
                "y": 143
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "30bc027c-25dd-4eca-83f8-617cb1849b50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 16,
                "x": 459,
                "y": 49
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "97835dc4-ee70-445f-a5cc-6c5ca61bff05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 2,
                "y": 96
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ca48d18f-c895-449d-87b1-0a16bd743ee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 236,
                "y": 96
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "797f93f8-8335-4b91-9ec0-ae728d0407e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 477,
                "y": 49
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f406d962-6b63-43dd-8666-c590bc56ffa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 218,
                "y": 96
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "500c9a36-61ab-410c-af88-5ce332b4d9c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 45,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 494,
                "y": 96
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "540705fd-b5d8-4623-8319-ce3b276b9a3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 200,
                "y": 96
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "70507a55-ee9c-45f6-9794-4f13f243283b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 45,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 182,
                "y": 96
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f60df9e3-c045-4bc4-af14-ed8c986d89ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 45,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 133,
                "y": 143
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "bd3ef6d1-106b-4f9a-99b3-494be26416d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 45,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 101,
                "y": 143
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f1bd2b04-0b68-4ea9-86f1-db80e12b5862",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 210,
                "y": 49
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2330f9bb-eeac-4389-b219-ddcafb5990d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 45,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 91,
                "y": 143
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "24da968c-ca90-4871-a462-520f096f4894",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 45,
                "offset": 2,
                "shift": 32,
                "w": 27,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d61fefab-9317-40f8-8efd-d66df14c9a77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 45,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 146,
                "y": 96
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3f87ba32-b40a-4c04-abbd-64366d96a9ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 128,
                "y": 96
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b62f8d3d-3ec4-4ba5-95c7-677fa9c3c73d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 74,
                "y": 96
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d3745dbf-8ca3-48f8-8068-0189b4a7d386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 56,
                "y": 96
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "bdcd52c1-eb95-4ec6-b528-061016b13024",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 45,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 391,
                "y": 96
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "3069b0a6-005b-45a0-885a-171682c08429",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 346,
                "y": 49
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9961e913-5ad4-40ca-8882-7a9b718fb809",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 45,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 49,
                "y": 143
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "df744bf6-01aa-466d-bf80-5bb7fd240075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 45,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 20,
                "y": 96
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "29d72d67-d563-4af1-867a-14d1b07efe98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 45,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 190,
                "y": 49
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "8f5d6983-5ddb-4f0b-bbb8-fb39ce017cb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 45,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a0f34ebf-f303-463c-984a-ce7a5261d721",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 308,
                "y": 49
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7187d704-0c98-4bfb-be00-8928cd536729",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 45,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 150,
                "y": 49
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "aef003d3-72b5-47c2-a892-02c7f99d7808",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 272,
                "y": 96
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "9904e7b7-30d5-42bb-8b5f-f5cc44a3d873",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 45,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 468,
                "y": 96
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "bb9beb55-c662-457f-8cf0-aabe2178bb51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 45,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 174,
                "y": 143
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "49355684-cbd6-435d-80b4-14f6f8a499d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 45,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 481,
                "y": 96
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a333210d-575c-4ed7-9801-9309f9d3ece0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 45,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 306,
                "y": 96
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 28,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}